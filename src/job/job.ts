import * as _ from 'lodash'
import * as moment from 'moment';
import {TYPES} from "./types";
import {STATUSES} from "./status";

export const DATE_FORMAT = 'DD MMM YYYY';

export interface JobDistance {
	text: string,
	value: number
}

export interface JobDuration {
	text: string,
	value: number
}

export interface JobItem {
	name: string,
	description: string
}

export interface JobCoordinates {
	lat: number,
	lng: number
}

export interface JobLocation {
	address: string,
	coordinates: JobCoordinates,
	placeId: string,
	stairs: number,
	lift: boolean,
	info: string
}

export class BaseJob {
  public id: string;
  public firstname: string;
  public lastname: string;
  public phone: string;
  public email: string;
	public from: JobLocation;
	public to: JobLocation;
	public items: JobItem[];
  public startDate: number;
  public endDate: number;
	public jobType: number;
	public price: number;
	public discountPrice: number;
	public distance: JobDistance;
	public duration: JobDuration;
	public confirmed: boolean;
	public councilVoucher: boolean;
	public createdAt: number;
	public confirmedAt: number;
	public updatedAt: number;
	public updatedBy: number;
	public status: number;
	public cancelReason: string;
	public offset: number;

  public static parseJobs(jobs: any[]): BaseJob[] {
    return jobs.map(job => new BaseJob(job));
  }

  constructor(job: any) {
    this.id = job.id;
    this.firstname = job.firstname;
    this.lastname = job.lastname;
    this.phone = job.phone;
    this.email = job.email;
	  this.from = job.from;
	  this.to = job.to;
	  this.items = job.items;
	  this.startDate = job.startDate;
	  this.endDate = job.endDate;
	  this.jobType = job.jobType;
	  this.price = job.price;
	  this.discountPrice = job.discountPrice;
	  this.distance = job.distance;
	  this.duration = job.duration;
	  this.confirmed = job.confirmed;
	  this.councilVoucher = job.councilVoucher;
	  this.createdAt = job.createdAt;
	  this.confirmedAt = job.confirmedAt;
	  this.updatedAt = job.updatedAt;
	  this.updatedBy = job.updatedBy;
	  this.status = job.status;
	  this.cancelReason = job.cancelReason;
	  this.offset = job.offset || 0;
  }

	/**
	 * Return current status name
	 * @returns {string}
	 */
  public getStatus(): string {
  	return _.find(STATUSES, status => status.id === this.status).label;
  }

  public getPaymentPrice(): number {
    return this.discountPrice || this.price;
  }

	public getDisplayStartDate(): string {
		return moment(this.startDate).format(DATE_FORMAT);
	}

	public getDisplayEndDate(): string {
		return moment(this.endDate).format(DATE_FORMAT);
	}

  public getDisplayType(): string {
	  return _.find(TYPES, status => status.id === this.jobType).label;
  }

  public getTimeToJob(): string {
    return moment().to(moment(this.startDate));
  }

  public isToday(): boolean {
    return moment().isSame(moment(this.startDate), 'd');
  }

  public isTomorrow(): boolean {
    return moment().add(1, 'day').isSame(moment(this.startDate), 'd');
  }
}