
export const STATUS = {
	QUOTE: 1,
	BOOKED: 2,
	CANCELLED: 3,
	ACCEPTED: 4,
	COMPLETED: 5,
	REFUNDED: 6
};

export interface Status {
	id: number,
	label: string
}

export let STATUSES: Status[] = [
	{ id: STATUS.QUOTE, label: 'Quote' },
	{ id: STATUS.BOOKED, label: 'Booked' },
	{ id: STATUS.CANCELLED, label: 'Cancelled' },
	{ id: STATUS.ACCEPTED, label: 'Accepted' },
	{ id: STATUS.COMPLETED, label: 'Completed' },
	{ id: STATUS.REFUNDED, label: 'Refunded' },
];


