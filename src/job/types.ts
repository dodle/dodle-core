
export const TYPE = {
	FURNITURE: 1,
	SM_HOUSE_APP: 2,
	RUBBISH: 3
};

export interface Type {
	id: number,
	label: string
}

export let TYPES: Type[] = [
	{ id: TYPE.FURNITURE, label: 'Furniture pickup/delivery' },
	{ id: TYPE.SM_HOUSE_APP, label: 'Small house/apartment move' },
	{ id: TYPE.RUBBISH, label: 'Rubbish Removal (Brisbane Only)' }
];
