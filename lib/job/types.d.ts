export declare const TYPE: {
    FURNITURE: number;
    SM_HOUSE_APP: number;
    RUBBISH: number;
};
export interface Type {
    id: number;
    label: string;
}
export declare let TYPES: Type[];
