export declare const DATE_FORMAT: string;
export interface JobDistance {
    text: string;
    value: number;
}
export interface JobDuration {
    text: string;
    value: number;
}
export interface JobItem {
    name: string;
    description: string;
}
export interface JobCoordinates {
    lat: number;
    lng: number;
}
export interface JobLocation {
    address: string;
    coordinates: JobCoordinates;
    placeId: string;
    stairs: number;
    lift: boolean;
    info: string;
}
export declare class BaseJob {
    id: string;
    firstname: string;
    lastname: string;
    phone: string;
    email: string;
    from: JobLocation;
    to: JobLocation;
    items: JobItem[];
    startDate: number;
    endDate: number;
    jobType: number;
    price: number;
    discountPrice: number;
    distance: JobDistance;
    duration: JobDuration;
    confirmed: boolean;
    councilVoucher: boolean;
    createdAt: number;
    confirmedAt: number;
    updatedAt: number;
    updatedBy: number;
    status: number;
    cancelReason: string;
    offset: number;
    static parseJobs(jobs: any[]): BaseJob[];
    constructor(job: any);
    getStatus(): string;
    getPaymentPrice(): number;
    getDisplayStartDate(): string;
    getDisplayEndDate(): string;
    getDisplayType(): string;
    getTimeToJob(): string;
    isToday(): boolean;
    isTomorrow(): boolean;
}
