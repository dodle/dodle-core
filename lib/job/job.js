"use strict";
var _ = require('lodash');
var moment = require('moment');
var types_1 = require("./types");
var status_1 = require("./status");
exports.DATE_FORMAT = 'DD MMM YYYY';
var BaseJob = (function () {
    function BaseJob(job) {
        this.id = job.id;
        this.firstname = job.firstname;
        this.lastname = job.lastname;
        this.phone = job.phone;
        this.email = job.email;
        this.from = job.from;
        this.to = job.to;
        this.items = job.items;
        this.startDate = job.startDate;
        this.endDate = job.endDate;
        this.jobType = job.jobType;
        this.price = job.price;
        this.discountPrice = job.discountPrice;
        this.distance = job.distance;
        this.duration = job.duration;
        this.confirmed = job.confirmed;
        this.councilVoucher = job.councilVoucher;
        this.createdAt = job.createdAt;
        this.confirmedAt = job.confirmedAt;
        this.updatedAt = job.updatedAt;
        this.updatedBy = job.updatedBy;
        this.status = job.status;
        this.cancelReason = job.cancelReason;
        this.offset = job.offset || 0;
    }
    BaseJob.parseJobs = function (jobs) {
        return jobs.map(function (job) { return new BaseJob(job); });
    };
    BaseJob.prototype.getStatus = function () {
        var _this = this;
        return _.find(status_1.STATUSES, function (status) { return status.id === _this.status; }).label;
    };
    BaseJob.prototype.getPaymentPrice = function () {
        return this.discountPrice || this.price;
    };
    BaseJob.prototype.getDisplayStartDate = function () {
        return moment(this.startDate).format(exports.DATE_FORMAT);
    };
    BaseJob.prototype.getDisplayEndDate = function () {
        return moment(this.endDate).format(exports.DATE_FORMAT);
    };
    BaseJob.prototype.getDisplayType = function () {
        var _this = this;
        return _.find(types_1.TYPES, function (status) { return status.id === _this.jobType; }).label;
    };
    BaseJob.prototype.getTimeToJob = function () {
        return moment().to(moment(this.startDate));
    };
    BaseJob.prototype.isToday = function () {
        return moment().isSame(moment(this.startDate), 'd');
    };
    BaseJob.prototype.isTomorrow = function () {
        return moment().add(1, 'day').isSame(moment(this.startDate), 'd');
    };
    return BaseJob;
}());
exports.BaseJob = BaseJob;
