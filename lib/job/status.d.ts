export declare const STATUS: {
    QUOTE: number;
    BOOKED: number;
    CANCELLED: number;
    ACCEPTED: number;
    COMPLETED: number;
    REFUNDED: number;
};
export interface Status {
    id: number;
    label: string;
}
export declare let STATUSES: Status[];
