"use strict";
exports.STATUS = {
    QUOTE: 1,
    BOOKED: 2,
    CANCELLED: 3,
    ACCEPTED: 4,
    COMPLETED: 5,
    REFUNDED: 6
};
exports.STATUSES = [
    { id: exports.STATUS.QUOTE, label: 'Quote' },
    { id: exports.STATUS.BOOKED, label: 'Booked' },
    { id: exports.STATUS.CANCELLED, label: 'Cancelled' },
    { id: exports.STATUS.ACCEPTED, label: 'Accepted' },
    { id: exports.STATUS.COMPLETED, label: 'Completed' },
    { id: exports.STATUS.REFUNDED, label: 'Refunded' },
];
